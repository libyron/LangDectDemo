from abc import ABCMeta, abstractmethod

class LangDectApproach:

	__metaclass__ = ABCMeta

	@abstractmethod
	def infer(doc):
		pass

	@abstractmethod
	def train():
		pass

	@abstractmethod
	def save_model():
		pass

	@abstractmethod
	def load_model():
		pass
	

